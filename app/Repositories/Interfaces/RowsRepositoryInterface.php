<?php

namespace App\Repositories\Interfaces;

/**
 * Интерфейс репозитория строк
 */
interface RowsRepositoryInterface
{
    /**
     * Получение всех записей
     *
     * @return mixed
     */
    public function all();

    /**
     * Получение дерева всех записей
     *
     * @return mixed
     */
    public function getTree();

    /**
     * Формирование списка элементов для вывода во view
     *
     * @return string
     */
    public function getTreeList(): string;
}