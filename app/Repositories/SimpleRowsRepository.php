<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RowsRepositoryInterface;

/**
 * Репозиторий для вывода простых данных из массива
 */
class SimpleRowsRepository implements RowsRepositoryInterface
{
    /** @var array */
    protected $data = [
        [
            'title'    => 'основание из песка',
            'value'    => '673.00',
            'position' => '2.1.1.'
        ],
        [
            'title'    => 'Утилизация грунта',
            'value'    => '1000.00',
            'position' => '1.3.'
        ],
        [
            'title'    => 'до 30 км',
            'value'    => '979.00',
            'position' => '1.2.3.'
        ],
        [
            'title'    => 'грунт, грунт замусоренный (техн.)',
            'value'    => '223.10',
            'position' => '1.3.2.'
        ],
        [
            'title'    => 'основание из ПГС',
            'value'    => '7895.00',
            'position' => '2.1.3.'
        ],
        [
            'title'    => 'Устройство основания',
            'value'    => '1123.00',
            'position' => '2.1.'
        ],
        [
            'title'    => 'Засыпка',
            'value'    => '7648.56',
            'position' => '1.1.'
        ],
        [
            'title'    => 'щебень фр.20-40',
            'value'    => '4112.00',
            'position' => '2.1.7.'
        ],
        [
            'title'    => 'Устройство основания ("пирога")',
            'value'    => '534.50',
            'position' => '2.'
        ],
        [
            'title'    => 'ПГС',
            'value'    => '735.00',
            'position' => '2.1.4.'
        ],
        [
            'title'    => 'Земляные работы',
            'value'    => '43.00',
            'position' => '1.'
        ],
        [
            'title'    => 'до 10 км',
            'value'    => '344.10',
            'position' => '1.2.1.'
        ],
        [
            'title'    => 'грунт, грунт замусоренный (экологич.чистый)',
            'value'    => '654.40',
            'position' => '1.3.1.'
        ],
        [
            'title'    => 'Устройство бетонной подготовки',
            'value'    => '113.00',
            'position' => '2.2.'
        ],
        [
            'title'    => 'Вывоз грунта',
            'value'    => '3554.00',
            'position' => '1.2.'
        ],
        [
            'title'    => 'щебень фр.40-70',
            'value'    => '983.00',
            'position' => '2.1.6.'
        ],
        [
            'title'    => 'основание из щебня',
            'value'    => '1345.00',
            'position' => '2.1.5.'
        ],
        [
            'title'    => 'до 50 км',
            'value'    => '1287.00',
            'position' => '1.2.5.'
        ],
        [
            'title'    => 'Разработка грунта комплексная (с учетом погрузки в автомобили-самосвалы, доработки вручную, планировки и т.п.), вывоз до 1 км',
            'value'    => '4123.00',
            'position' => '1.1.'
        ],
        [
            'title'    => 'песок',
            'value'    => '123.00',
            'position' => '2.1.2.'
        ],
        [
            'title'    => 'до 40 км',
            'value'    => '1045.00',
            'position' => '1.2.4.'
        ],
        [
            'title'    => 'до 20 км',
            'value'    => '567.43',
            'position' => '1.2.2.'
        ],
        [
            'title'    => 'щебень фр.5-20',
            'value'    => '6783.00',
            'position' => '2.1.8.'
        ],
    ];

    protected $result = [];

    /**
     * Получение всех записей
     *
     * @return array
     */
    public function all(): array
    {
        return $this->getSortedData();
    }

    /**
     * Получение дерева всех записей
     *
     * @return array
     */
    public function getTree(): array
    {
        return $this->getGroupedData();
    }

    /**
     * Формирование списка элементов для вывода во view
     *
     * @return string
     */
    public function getTreeList(): string
    {
        $rows = $this->getTree();
        $result = '';

        foreach ($rows as $row) {
            $result .= $this->getListElement($row);
        }

        return '<ol>' . $result . '</ol>';
    }

    /**
     * Рекурсивное формирование элемента списка для вывода во view
     *
     * @param array $row
     *
     * @return string
     */
    protected function getListElement($row)
    {
        $result = "<li>{$row['position']} {$row['title']} <b>{$row['value']} ₽</b>";

        if (!empty($row['childs'])) {
            $result .= '<ol>';
            foreach ($row['childs'] as $childRow) {
                $result .= $this->getListElement($childRow);
            }
            $result .= '</ol>';
        }

        return $result . '</li>';
    }

    /**
     * Возвращает отсортированыые данные
     *
     * @param string $direction направление сортировки, ASC\DESC
     *
     * @return array
     */
    protected function getSortedData(string $direction = 'ASC')
    {
        $result = [];
        $sort = array_column($this->data, 'position');
        $direction === 'ASC' ? asort($sort) : arsort($sort);

        foreach ($sort as $key => $value) {
            $result[$value] = $this->data[$key];
        }

        return $result;
    }

    /**
     * Возвращает сгруппированные данные
     *
     * @return array
     */
    protected function getGroupedData() : array
    {
        $data = $this->getSortedData();
        $result = [];
        foreach ($data as $position => $value) {
            $keys = explode('.', $position);
            $keys = array_filter($keys);

            if (count($keys) === 1) {
                $key = array_shift($keys) . '.';
                $result[$key] = $value;
                unset($data[$key]);
                $result[$key]['childs'] = $this->findChilds($key, $data);
            }
        }

        return $result;
    }

    /**
     * Поиск дочерних элементов по ключу родителя
     *
     * @param $key
     * @param $arr
     *
     * @return array
     */
    protected function findChilds($key, $arr)
    {
        $result = [];
        foreach ($arr as $position => $value) {
            if (strpos($position, $key) === 0) {
                $pos = substr($position, 0, -strlen($key));

                if ($pos != '' && strlen($pos) == 2) {
                    $result[$position] = $value;
                    unset($arr[$position]);
                    $result[$position]['childs'] = $this->findChilds($position, $arr);
                }
            }
        }

        return $result;
    }
}