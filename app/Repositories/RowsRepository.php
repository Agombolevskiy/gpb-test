<?php

namespace App\Repositories;

use App\Models\Row;
use App\Repositories\Interfaces\RowsRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Репозиторий записей БД
 */
class RowsRepository implements RowsRepositoryInterface
{
    /** @var Row */
    protected $modelClass;

    /**
     * RowsRepository constructor.
     */
    public function __construct()
    {
        $this->modelClass = Row::class;
    }

    /**
     * Получение всех записей
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->modelClass::all();
    }

    /**
     * Получение дерева всех записей
     *
     * @return Collection
     */
    public function getTree(): Collection
    {
        return $this->modelClass::with(['childRows', 'childRows.childRows', 'parentRow'])
                                ->whereNull('parent_id')
                                ->get();
    }

    /**
     * Формирование списка элементов для вывода во view
     *
     * @return string
     */
    public function getTreeList(): string
    {
        $rows = $this->getTree()->sortBy('position');
        $result = '';

        foreach ($rows as $row) {
            $result .= $this->getListElement($row);
        }

        return '<ol>' . $result . '</ol>';
    }

    /**
     * Рекурсивное формирование элемента списка для вывода во view
     *
     * @param Row $row
     *
     * @return string
     */
    protected function getListElement(Row $row)
    {
        $result = "<li>{$row->position}. {$row->title} <b>{$row->value}</b>";

        if ($row->childRows->isNotEmpty()) {
            $result .= '<ol>';
            foreach ($row->childRows->sortBy('position') as $childRow) {
                $result .= $this->getListElement($childRow);
            }
            $result .= '</ol>';
        }

        return $result . '</li>';
    }
}