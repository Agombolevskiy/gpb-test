<?php

namespace App\Http\Controllers;

use App\Repositories\RowsRepository;
use App\Repositories\SimpleRowsRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Контроллер
 */
class RowsController extends Controller
{
    /** @var RowsRepository */
    protected $repository;

    /**
     * RowsController constructor.
     */
    public function __construct()
    {
        $this->repository = new RowsRepository();
    }

    /**
     * Отображение страницы со всеми записями
     *
     * @return Factory|View
     */
    public function index()
    {
        $rows = $this->repository->getTreeList();
        // Для примера добавлен репозиторий с массивом данных
        $simpleRows = (new SimpleRowsRepository())->getTreeList();

        return view('rows.index', compact('rows', 'simpleRows'));
    }
}