<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

/**
 * Модель
 *
 * @property int                           $id
 * @property int                           $parent_id
 * @property int                           $position
 * @property string                        $title
 * @property string                        $value
 *
 * @property-read EloquentCollection|Row[] $childRows
 * @property-read int|null                 $child_rows_count
 * @property-read Row                      $parentRow
 *
 * @method static Builder|Row whereParent($id)
 * @method static Builder|Row withoutParent()
 *
 * @mixin \Eloquent
 */
class Row extends Model
{
    protected $casts = [
        'value' => 'float'
    ];

    public $timestamps = false;

    /**
     * Связь с родительской строкой
     *
     * @return BelongsTo|Builder
     */
    public function parentRow()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * Связь с дочерними строками
     *
     * @return BelongsTo|Builder
     */
    public function childRows()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    /**
     * Скоуп для выборки только строк первого уровня(без родителя)
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeWithoutParent(Builder $builder)
    {
        return $builder->whereNull('parent_id');
    }


    /**
     * Скоуп для выборки строк по id родителя
     *
     * @param Builder   $builder
     * @param int|int[] $id
     *
     * @return Builder
     */
    public function scopeWhereParent(Builder $builder, $id)
    {
        if (is_array($id) || $id instanceof Collection) {
            return $builder->whereIn('parent_id', $id);
        }

        return $builder->where('parent_id', $id);
    }

    /**
     * Мутатор для получения атрибута value
     *
     * @param string $value
     *
     * @return string
     */
    public function getValueAttribute($value)
    {
        return $value . ' ₽';
    }

    /**
     * Мутатор для получения атрибута value
     *
     * @param string $value
     *
     * @return string
     */
    public function getPositionAttribute($value)
    {
        return $this->parentRow ? $this->parentRow->position . '.' . $value : $value;
    }
}
