<?php

use Illuminate\Database\Seeder;
use App\Models\Row;
use Illuminate\Support\Facades\DB;

class RowsSeeder extends Seeder
{
    protected $busyIds = [];

    protected $rows = [
        [
            'position'  => 1,
            'title'     => 'Земляные работы',
            'value'     => 43.00,
            'childs'    => [
                [
                    'position'  => 1,
                    'title'     => 'Разработка грунта комплексная (с учетом погрузки в автомобили-самосвалы, доработки вручную, планировки и т.п.), вывоз до 1 км',
                    'value'     => 4123.00
                ],
                [
                    'position'  => 2,
                    'title'     => 'Вывоз грунта',
                    'value'     => 3554.00,
                    'childs'    => [
                        [
                            'position'  => 1,
                            'title'     => 'до 10 км',
                            'value'     => 344.10
                        ],
                        [
                            'position'  => 2,
                            'title'     => 'до 20 км',
                            'value'     => 567.43
                        ],
                        [
                            'position'  => 3,
                            'title'     => 'до 30 км',
                            'value'     => 979.00
                        ],
                        [
                            'position'  => 4,
                            'title'     => 'до 40 км',
                            'value'     => 1045.00
                        ],
                        [
                            'position'  => 5,
                            'title'     => 'до 50 км',
                            'value'     => 1287.00
                        ],
                    ]
                ],
                [
                    'position'  => 3,
                    'title'     => 'Утилизация грунта',
                    'value'     => 1000.00,
                    'childs'    => [
                        [
                            'position'  => 1,
                            'title'     => 'грунт, грунт замусоренный (экологич.чистый)',
                            'value'     => 654.40
                        ],
                        [
                            'position'  => 2,
                            'title'     => 'грунт, грунт замусоренный (техн.)',
                            'value'     => 223.10
                        ],
                    ]
                ],
                [
                    'position'  => 1.4,
                    'title'     => 'Засыпка',
                    'value'     => 7648.56
                ],
            ]
        ],
        [
            'position'  => 2,
            'title'     => 'Устройство основания ("пирога")',
            'value'     => 534.50,
            'childs'    => [
                [
                    'position'  => 1,
                    'title'     => 'Устройство основания',
                    'value'     => 1123.00,
                    'childs'    => [
                        [
                            'position'  => 1,
                            'title'     => 'основание из песка',
                            'value'     => 673.00
                        ],
                        [
                            'position'  => 2,
                            'title'     => 'песок',
                            'value'     => 123.00
                        ],
                        [
                            'position'  => 3,
                            'title'     => 'основание из ПГС',
                            'value'     => 7895.00
                        ],
                        [
                            'position'  => 4,
                            'title'     => 'ПГС',
                            'value'     => 735.00
                        ],
                        [
                            'position'  => 5,
                            'title'     => 'основание из щебня',
                            'value'     => 1345.00
                        ],
                        [
                            'position'  => 6,
                            'title'     => 'щебень фр.40-70',
                            'value'     => 983.00
                        ],
                        [
                            'position'  => 7,
                            'title'     => 'щебень фр.20-40',
                            'value'     => 4112.00
                        ],
                        [
                            'position'  => 8,
                            'title'     => 'щебень фр.5-20',
                            'value'     => 6783.00
                        ],
                    ]
                ],
                [
                    'position'  => 2,
                    'title'     => 'Устройство бетонной подготовки',
                    'value'     => 113.00
                ],
            ]
        ],
    ];

    /**
     * Запуск сидера
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('rows')->truncate();
        foreach ($this->rows as $row) {
            $this->insertRow($row);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }

    /**
     * Рекурсивно вставляет записи в БД
     *
     * @param array    $row
     * @param int|null $parentId
     */
    protected function insertRow(array $row, ?int $parentId = null)
    {
        $childs = Arr::pull($row, 'childs', []);
        $row['parent_id'] = $parentId;
        $row['id'] = $this->getRandId();

        $rowId = Row::insertGetId($row);
        $this->busyIds[] = $rowId;

        foreach ($childs as $child) {
            $this->insertRow($child, $rowId);
        }
    }

    /**
     * Возвращает рандомный ID для новой записи.
     * Метод сделан для того чтоб сбить порядок записей
     */
    protected function getRandId(): int
    {
        $id = rand(1, 9999);

        return in_array($id, $this->busyIds, true) ? $this->getRandId() : $id;
    }
}
