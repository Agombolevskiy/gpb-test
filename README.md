## Тестовое задание для "Электронная торговая площадка Газпромбанка"

1. Создание таблицы для хранения в БД `database/migrations/2020_05_20_122016_create_rows_table.php`
2. Наполнение таблицы `database/seeds/RowsSeeder.php`
3. Модель `app/Models/Row.php`
4. Контроллер `app/Http/Controllers/RowsController.php`
5. Получение данных из БД `app/Repositories/RowsRepository.php`
6. Получение данных из массива `app/Repositories/SimpleRowsRepository.php`

#### Формирование верстки в репозиториях сделано осознано, для облегчения проверки задания. 